package com.spring.db;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * This class is required for connect to database.
 * */

public class DBProcessor {
	
	public static Connection conn;
	
	public void connectDB()
	{
		
		try {
			
			Class.forName("org.postgresql.Driver");
			
			String pass="root";
			String user="postgres";
			
			String url="jdbc:postgresql://localhost:5432/User";
			
			
			conn=DriverManager.getConnection(url,user,pass);
		
			
		} catch (Exception e) {
			
		}
		
	}

}
