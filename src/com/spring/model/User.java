package com.spring.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class implements a simple user 
 * @author SELIN GOK
 * @version 1.0
 * */

@XmlRootElement(namespace="com.spring.model")
public class User {
	
	private int id;
	private String username;
	private String password;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	

}
