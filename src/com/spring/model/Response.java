package com.spring.model;

/**
 * This class is used at the end of each operation for providing information to the user.
 * */

public class Response<T>{
	private int status;
	private String message;
	private Object data;
	
	/**
	 * This constructs a response with status, message and data
	 * @param status status of the user's request
	 * @param message status message
	 * @param data which object is getting, it can be null
	 * */
	public Response(int status, String message, Object data) {
		this.status=status;
		this.message=message;
		this.data=data;
	}
	
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	
}
