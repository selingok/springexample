package com.spring.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.dao.LoginDAO;
import com.spring.model.Response;
import com.spring.model.User;

/**
 * This service makes the user operations.
 * <ul>Which operations?
 * <li>User registration
 * <li>Username control
 * <li>Login
 * <li>Getting list of user
 * </ul>
 * */

@Controller
@RequestMapping("/loginservice")
public class LoginController {
	
	User user = new User();
	
	/**
	 * Just control of connection to the user web service
	 * */
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody void User(){
		System.out.println("BAGLANTI BASARILI");
	}
	
	
	/**
	 * The user is saved in the database.
	 * @param user user's information must be JSON format.
	 * @return this returns a response object which has a status, a message and maybe a data
	 * */
	@RequestMapping(value="/add" ,method=RequestMethod.POST)
	public @ResponseBody Response<User> addUser(@RequestBody User user) throws SQLException{
		
		int update = new LoginDAO().add(user);	
		if(update != 0)
			return new Response<User>(200,"Completed successfully",user);
		else
			return new Response<User>(500,"Fail",null);
	}
	
	
	
	/**
	 * username's availability is controlled.
	 * @param username it is a user name which user wants
	 * @return this returns a response object which has a status, a message and maybe a data
	 * */
	@RequestMapping(value="/accountcontrol/{username}",method=RequestMethod.GET)
	public @ResponseBody Response<User> accountControl(@PathVariable String username) throws SQLException{

		boolean status = new LoginDAO().accountControl(username);
		
		if(status)
			return new Response<User>(200,"Username kullanilabilir",null);
		else
			return new Response<User>(404,"Username baskasi tarafindan kullaniliyor.",null);
	}
	

	/**
	 * Login service
	 * @param user user's information must be JSON format.
	 * @return this returns a response object which has a status, a message and maybe a data
	 * */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public @ResponseBody Response<User> login(@RequestBody User user) throws SQLException{
		
		boolean status = new LoginDAO().login(user);
		
		if(status)
			return new Response<User>(200,"Welcome!",user);
		else
			return new Response<User>(404,"Please check your username and password",null);
	}
	

	/**
	 * Getting list of user
	 * @return list of users in JSON format.
	 * */
	@RequestMapping(value="/userlist",method = RequestMethod.GET)
	public @ResponseBody List<User> printUserList() throws SQLException{
		
		List<User> userList = new ArrayList<User>();
		
		userList = new LoginDAO().getList();
		
		return userList;

	}
		

}
