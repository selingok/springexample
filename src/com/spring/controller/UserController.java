package com.spring.controller;


import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.dao.UserDAO;
import com.spring.model.Response;
import com.spring.model.User;

@Controller
@RequestMapping("/user")
public class UserController {
	

	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public @ResponseBody Response<User> deleteAccount(@RequestBody User user) throws SQLException{
		int update =new UserDAO().drop(user);
		if(update != 0)
			return new Response<User>(200,"Account deleted",null);
		else
			return new Response<User>(404,"Fail",null);

	}
	
	@RequestMapping(value="/update/{newPass}", method=RequestMethod.POST)
	public @ResponseBody Response<User> updatePassword(@RequestBody User user, @PathVariable String newPass) throws SQLException{
			int update = new UserDAO().update(user, newPass);
			if(update != 0)
				return new Response<User>(200,"Password changed",null);
			else
				return new Response<User>(404,"Fail",null);

	}
	

}
