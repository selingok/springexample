package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.spring.db.DBProcessor;
import com.spring.model.User;

/**
 * SQL operations are made in this class.
 * */

public class LoginDAO extends DBProcessor {
	
	private String sql;
	private DataSource dataSource; 	//JDBCTEMPLATE KULLANMADIM KALDIRABILIRIM
	private JdbcTemplate jdbcTemplate;

	/**
	 * This method saves the user to the database.
	 * @param user it takes a user
	 * @throws SQLException for SQL exceptions
	 * @return it returns a boolean expression, if executing is successful it will return true else it will return false
	 * */
	public int add(User user) throws SQLException{
		
		sql = "INSERT INTO userlist (username,password) VALUES ('"+user.getUsername()+"','"+user.getPassword()+"')";
		connectDB();		
		Statement stmt=conn.createStatement();		
		return stmt.executeUpdate(sql);
		
	}
	
	
	/**
	 * username's availability is controlled by this method.
	 * @param username it is a user name which user wants
	 * @return if that username is available this method will return true, else false.
	 * */
	public boolean accountControl(String username) throws SQLException{
		
		List<User> userList = new ArrayList<User>();
		userList = getList();
		
		for(int i=0 ; i<userList.size(); i++){
			if(userList.get(i).getUsername().equals(username) )
				return false;	
		}
		return true;
	}
	
	
	/**
	 * This method gets the user list from database.
	 * @return list of users
	 * */
	public List<User> getList() throws SQLException
	{	
		
		String sql="SELECT * FROM userList" ;

		connectDB();		
		
		Statement stmt=conn.createStatement();		
		ResultSet set=stmt.executeQuery(sql);
	
		List<User> liste=new ArrayList<User>();
		
		while(set.next())
		{
			User user=new User();

			user.setId(set.getInt("id"));
			user.setUsername(set.getString("username"));
			user.setPassword(set.getString("password"));
	
			liste.add(user);
					
		}
				
		return liste;	
	}
	
	/**
	 * This method ensure that if username and password are true, the user can login the system.
	 * @param user the user who want to login the system
	 * @return if the user's informations are true it will returns true, else false.
	 * */
		public boolean login(User user) throws SQLException{
			
			String sql="SELECT * FROM userlist "
					+ "WHERE username='"+user.getUsername()+"' "
							+ "AND password='"+user.getPassword()+"' " ;
		
			connectDB();		
			
			Statement stmt=conn.createStatement();		
			ResultSet set=stmt.executeQuery(sql);
			
			if(set.next())
				return true;
			else
				return false;
		
		}
	
	//GETTER SETTERS
	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

}
