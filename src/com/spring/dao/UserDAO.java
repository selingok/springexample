package com.spring.dao;

import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import com.spring.db.DBProcessor;
import com.spring.model.User;

public class UserDAO extends DBProcessor {
	
	private String sql;
	private DataSource dataSource;
	
	/**
	 * 
	 * @param user
	 * @return the number of rows which changing. if the number equals 0 that means this query is not success
	 * @throws SQLException
	 */
	
	public int drop(User user) throws SQLException{
		
		sql = "delete from userlist where username = '"+user.getUsername()+"'"
				+ " and password='"+user.getPassword()+"'";

		connectDB();
		Statement stmt = conn.createStatement();
		return stmt.executeUpdate(sql);		
	}
	
	
	public int update(User user, String newPass) throws SQLException{
		
		sql = "update userlist set password = '"+newPass+"'"
				+ " where username = '"+user.getUsername()+"'"
						+ "and password = '"+user.getPassword()+"'";
		
		connectDB();
		Statement stmt = conn.createStatement();
		return stmt.executeUpdate(sql);
	}
	
	
	
	
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	

}
